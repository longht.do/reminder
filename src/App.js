import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';

function App() {
  return (
    <div className="App">
      <h3>Reminders</h3>
      <br/>
      <h3>Add Reminder</h3>
      <form>
        <label htmlFor="id">Id</label>
        <input name="id" placeholder="id"/>
        <label htmlFor="id">Reminder</label>
        <input name="reminder" placeholder="Reminder"/>
        <label htmlFor="time">Time</label>
        <input name="time" placeholder="Time"/>
        <button type='submit'>Add</button>
      </form>
    </div>
  );
}

export default App;
